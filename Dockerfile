FROM python:3-alpine

COPY . /src
RUN pip install -r /src/requirements.txt

WORKDIR /src
ENV PYTHONPATH '/src/'

EXPOSE     8000
CMD ["python", "/src/app.py"]

